from __future__ import unicode_literals
from mimetypes import MimeTypes

from bson import ObjectId
from gridfs import GridFS
from pymongo import MongoClient

from flask import Flask, make_response, render_template, request
from flask.json import dumps, loads
import tempfile

from pyav import Media
from metadata import MetadataManager, MetadataFile

import Image
import ctypes
import array

app = Flask(__name__)

db = MongoClient().vidi
fs = GridFS(db)


def _ptr_add(ptr, offset):
    address = ctypes.addressof(ptr.contents) + offset
    return ctypes.pointer(type(ptr.contents).from_address(address))


def get_video_info(fn):
    m = Media(fn)
    return m.info()


def get_item(item_id):
    return fs.get(ObjectId(item_id))


def get_thumbnail(fn):
    m = Media(fn)
    info = m.info()

    # select first video stream
    vstreams = [ i for i, s in enumerate(info['stream']) if s['type'] == 'video' ]
    if vstreams:
        vstream = vstreams[0]
    streamInfo = info['stream'][vstream]
    size = streamInfo['width'], streamInfo['height']
    m.addScaler2(vstream, *size)

    for p in m:
        if p.streamIndex() == vstream:
            p.decode()
            if p.decoded:
                frame = p.swsFrame

                # Convert frame to PPM image
                with tempfile.NamedTemporaryFile(delete=False) as f:
                    f = open(f.name, 'wb')
                    w, h = size
                    a = array.array(b'B', b'\0'*(w*3))
                    f.write(b'P6\n%d %d\n255\n' % (w, h))
                    for i in xrange(h):
                        ptr = _ptr_add(frame.contents.data[0], i*frame.contents.linesize[0])
                        ctypes.memmove(a.buffer_info()[0], ptr, w*3)
                        a.tofile(f)
                    f.flush()

                    im = Image.open(f.name)
                    max_res = (300, 300)
                    if im.size[0] > max_res[0] or im.size[1] > max_res[1]:
                        factor = max(im.size[0]/float(max_res[0]), im.size[1]/float(max_res[1]))
                        im = im.resize(
                            (int(im.size[0]/factor), int(im.size[1]/factor)),
                            Image.ANTIALIAS
                        )
                    with tempfile.NamedTemporaryFile() as f2:
                        im.save(f2.name, format='jpeg')
                        return f2.read()


@app.route('/item/<item_id>/thumbnail/', methods=['GET'])
def item_thumbnail(item_id):
    item_obj = get_item(item_id)
    thumbnail_obj = get_item(item_obj.thumbnail)

    response = make_response(thumbnail_obj.read())
    response.headers['Content-Type'] = 'image/jpeg'
    return response


@app.route('/item/<path:filename>', methods=['PUT'])
def item_upload(filename):
    content = request.stream.read()
    with tempfile.NamedTemporaryFile() as f:
        f.write(content)
        f.flush()
        streaminfo = get_video_info(f.name)

        thumbnail = get_thumbnail(f.name)

    thumbnail_id = unicode(fs.put(thumbnail))
    new_item_id = unicode(fs.put(content, filename=filename, streaminfo=streaminfo, thumbnail=thumbnail_id, metadata={}))

    md_file = MetadataFile({'title': filename, 'id': new_item_id})
    MetadataManager().save_metadata(md_file)

    return dumps({'status': 'ok', 'item_id': new_item_id})


@app.route('/search/<query>', methods=['GET'])
def search(query):
    mm = MetadataManager()
    search_resutls = mm.search(query)

    response = make_response(dumps(search_resutls))
    response.headers['Content-Type'] = 'application/json'
    return response


@app.route('/item/<item_id>', methods=['GET'])
def item(item_id):
    content = get_item(item_id)

    response = make_response(content.read())
    response.headers['Content-Type'], __ = MimeTypes().guess_type(
        content.filename)
    response.headers['Content-Disposition'] = (
        'attachment; filename=%s' % content.filename
    )

    return response


@app.route('/item/<item_id>/metadata/', methods=['GET'])
def item_metadata(item_id):
    item_obj = get_item(item_id)

    response = make_response(dumps({
        'filename': item_obj.filename,
        'streaminfo': item_obj.streaminfo
    }))
    response.headers['Content-Type'] = 'application/json'
    return response


@app.route('/')
def index():
    mm = MetadataManager()
    items = mm.search('*')
    return render_template('index.html', items=items)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
