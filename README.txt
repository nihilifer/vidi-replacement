The ffmpeg interface doesn't work with the current mac ffmpeg,
we used the vagrant machine declared in the included Vagrant file,
it doesn't do setup tough.

Things to do to install:
sudo apt-get update
sudo apt-get install python ffmpeg libjpeg-dev mongodb libxslt-dev python-dev default-jre -y
sudo ln -s libavcodec.so.53 /usr/lib/x86_64-linux-gnu/libavcodec.so

Make a virtualenv with requirements.txt installed, boot the app with python app.py

In a separate ssh session:
Download solr(4.7.0) and untar
go into solr-4.7.0/example
Copy _misc/solr/schema.xml to solr/collection1/conf/schema.xml
run solr with java -jar start.jar

You can upload with:
curl --upload-file <path-to-file> http://<ip>:5000/item/<filename>

Interface:
http://<ip>:5000/ < index
http://<ip>:5000/search/<query> (example: *) < search
http://<ip>:5000/item/<id> < download file
http://<ip>:5000/item/<id>/metadata < metadata
http://<ip>:5000/item/<id>/thumbnail < thumbnails
