import uuid
import pysolr

solr = pysolr.Solr('http://localhost:8983/solr/', timeout=10)


class MetadataFile(object):
    def __init__(self, data={}):
        if not 'id' in data:
            data = data.copy()
            data['id'] = str(uuid.uuid4())

        self.data = data


class MetadataManager(object):
    def create_metadata_file(self):
        return MetadataFile()

    def save_metadata(self, md_file):
        mf_data = md_file.data

        solr.add([mf_data])

    def search(self, query):
        results = solr.search(query)
        return results.docs
